/* 
Scripts waits for message from extension and then run listener methods 
*/

// functions that clears cookies
function clearCookies(){
	var cookies = document.cookie;
	// decode special characters accroding to w3c
  var decoded_cookies = decodeURIComponent(cookies);
  var cookies_arr = decoded_cookies.split(';');
  // create old Date e.g. January 1 1970
  var d = new Date();
  var expires = "expires="+d.toUTCString();
  var path="path=/";
  
  //Delete (expire) all cookies
  if (cookies_arr.length > 0){
  cookies_arr.forEach(function(value,index){
    var key_val_arr = value.split('=');
    if (key_val_arr.length == 2){
      var key = key_vall_arr[0];
      var value = key_vall_arr[1];
      document.cookie = key+"="+";"+expires+";"+path;
    }
  });
  }
  
  // reload page
  location.reload();
  
 
}


// when message from extension is recieved run method specified in parameter (e.g. clearcookies)
browser.runtime.onMessage.addListener(clearCookies);